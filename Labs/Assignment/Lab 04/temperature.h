#ifndef TEMPERATURE_H_INCLUDED
#define TEMPERATURE_H_INCLUDED

typedef unsigned char uint8;

typedef struct weather
{
  uint8 temperature;
  uint8 humidity;
};

struct weather arr_weather[30];
//struct weather* p_arr_weather;
//p_arr_weather = &arr_weather;

void SetWeatherInfo (uint8 day, uint8* temp, uint8* humidity);
void GetWeatherInfo (uint8 day);

#endif // TEMPERATURE_H_INCLUDED
