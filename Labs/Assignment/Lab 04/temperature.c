#include "temperature.h"

void SetWeatherInfo (uint8 day, uint8* temp, uint8* humidity)
{
    arr_weather[day-1].temperature = temp;
    arr_weather[day-1].humidity = humidity;
}
void GetWeatherInfo (uint8 day)
{
    printf("On day %d:\n", day);
    printf("The temperature is %d degree.\n", arr_weather[day-1].temperature);
    printf("The humidity is %d.\n\n", arr_weather[day-1].humidity);
}
