/*
==================================================================================
Author: Marcellino Ashraf
Name: Lab 04
Target: Linear and binary search
==================================================================================
Description:
+
==================================================================================
*/

#define MAX_ARRAY_SIZE 1001

#include <stdio.h>
#include <stdlib.h>

int main()
{
    int rand_array[MAX_ARRAY_SIZE];
    int *p_rand_array;
    p_rand_array = &rand_array;
    int i = 0, key;

    for(i; i < MAX_ARRAY_SIZE; i++)
    {
        rand_array[i] = i;
    }

    while (1)
    {
        printf("Please enter a number to find.\n");
        scanf("%d", &key);

        //Linear search
        for (i=0; i < MAX_ARRAY_SIZE; i++)
        {
            if (key == rand_array[i])
            {
                printf("Key found!!\n");
                printf("Key's index is %d\n", i);
                break;
            }
        }
        if (i == MAX_ARRAY_SIZE) printf("Key is not found.\n\n");
        printf("Number of iterations is %d.\n\n", i+1);

        //Binary search
       /* int high = MAX_ARRAY_SIZE, low = 0, medium;
        for (i = 0; i < MAX_ARRAY_SIZE; i++)
        {
            medium = (high + low)/2;
            if (key == rand_array[medium])
            {
                printf("Key found!!\n");
                printf("Key's index is %d\n", medium);
                break;
            }
            else
                {
                    if (key > rand_array[medium]) low = medium;
                    if (key < rand_array[medium]) high = medium;
                }
        }
        if (i == MAX_ARRAY_SIZE) printf("Key is not found.\n\n");
        printf("Number of iterations is %d.\n\n", i+1);*/
    }

    return 0;
}
