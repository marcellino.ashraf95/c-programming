/*
==================================================================================
Author: Marcellino Ashraf
Name: Lab 04
Target: Arrays and data structures
==================================================================================
Description:
+ Write a program that has "Lab Test" as a variable, and you have to copy it to
  another variable.
==================================================================================
constraint: Use sizeof operator in your solution
==================================================================================
*/

#include <stdio.h>
#include <stdlib.h>

int main()
{
    char arr_string_1[20] = {"Lab Test"};
    char arr_string_2[sizeof(arr_string_1)];
    int i=0;
    for(i; i < sizeof(arr_string_1); i++)
    {
        arr_string_2[i] = arr_string_1[i];
    }
    printf("The old variable is \"%s\".\n", arr_string_1);
    printf("The new variable is \"%s\".\n", arr_string_2);
    return 0;
}
