/*
==================================================================================
Author: Marcellino Ashraf
Name: Lab 04
Target: Arrays and data structures
==================================================================================
Description:
Implement a code the provides Set Weather info, and Get Weather info.
+ Create structure type with the elements temperature and humidity level.
+ Create an array of 30 elements holding 30 structure elements for the weather of
  a month.
+ Create void SetWeatherInfo (uint8 day, uint8* temp, uint8* humidity);
+ Create void GetWeatherInfo (uint8 day, uint8* temp, uint8* humidity);
==================================================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include "temperature.h"

int main()
{
     uint8 temp;
     uint8 day;
     uint8 humidity;
     uint8* p_temp;
     uint8* p_humidity;
     p_temp = &temp;
     p_humidity = &humidity;
     while (1)
     {
         printf("Please enter the day in the month to set its weather.\n");
         printf("Day: ");
         scanf("%d", &day);
         printf("Temperature: ");
         scanf("%d", p_temp);
         printf("Humidity: ");
         scanf("%d", p_humidity);
         SetWeatherInfo(day, p_temp, p_humidity);
         GetWeatherInfo(day);
     }

    return 0;
}
