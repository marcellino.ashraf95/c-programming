#ifndef DATABASE_H_INCLUDED
#define DATABASE_H_INCLUDED

#include <stdbool.h>

#define MAX_DATA_ENTRIES 10
#define MAX_NUMBER_OF_SUBJECTS 3

//typedef unsigned char int;

/**define structure**/
typedef struct
{
    int student_ID;
    int student_year;
    int Course_ID[MAX_NUMBER_OF_SUBJECTS];
    int Course_Grades[MAX_NUMBER_OF_SUBJECTS];
}data;

data *ptr_arr_data_start;       /**global pointer to point at the start of the array of data structures**/
data *ptr_arr_data_current;     /**global pointer to point at the first empty entry of the array**/
data *ptr_arr_data_read;        /**global pointer to point at the entry to be read without changing the current pointer**/

/**functions prototypes**/
bool SDB_IsFull (void);
int SDB_GetUsedSize(void);
bool SDB_AddEntry(int, int, int*, int*);
void SDB_DeleteEntry(int);
bool SDB_ReadEntry(int);
bool SDB_IsIdExist(int);

#endif // DATABASE_H_INCLUDED
