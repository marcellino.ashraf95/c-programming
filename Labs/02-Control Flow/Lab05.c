/*
 ============================================================================
 Name        : Lab05.c
 Author      : Marcellino Ashraf
 ========================================================
 Implement a program that takes an input value from the user, and
 prints the factorial value
 * Use �while� statement
 ============================================================================
*/

#include <stdio.h>

int main()
{
	while (1)
    {
        int input;
        double factorial=1;
        int counter=1;
        printf("Please enter a number to calculate its factorial.\n");
        scanf("%d", &input);
        counter = input;

        while (counter >= 0)
        {
            if (counter < 2)
            {
                factorial *= 1;
                break;
            }
            factorial *= counter;
            counter --;
        }
        printf("The factorial of %d is %.0f\n", input, factorial);
    }
	return 0;
}
