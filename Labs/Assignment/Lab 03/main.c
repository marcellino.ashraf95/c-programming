/*
==================================================================================
Author: Marcellino Ashraf
Name: Lab 03
Target: Addition overflow
==================================================================================
Description:
Design a program composed of the following:
+ Math library:
    - MathLib.c and MathLib.h.
    - unsigned char Math_Add(unsigned char param_1, unsigned char param_2);
+ Error log:
    - ErrorLog.c and ErrorLog.h.
    - void ErrLog_Log(void);
+ Main:
    - main.c.
    - Takes 2 8-bit variables from the user.
    - Call Math_Add().
    - Detect if the value returned is a valid addition value or not.
    - If the value is invalid, then report the error through calling ErrLog_Log();
+ Hint: Error can be caused due to addition overflow.
==================================================================================
*/

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "MathLib.h"
#include "ErrorLog.h"

int main()
{
    int input_1 = 0, input_2 = 0, sum = 0;

    while (1)
    {
        printf("Please enter 2 numbers to be added.\n");
        printf("Input 1: ");
        scanf("%d", &input_1);
        printf("Input 2: ");
        scanf("%d", &input_2);
        sum = input_1 + input_2;
        if (sum > 255)
            ErrLog_Log();
        else
            printf("%d + %d = %d\n\n", input_1, input_2, Math_Add(input_1, input_2));

    }

    return 0;
}
