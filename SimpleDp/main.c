/**
                     SIMPLE DATABASE PROJECT
               AUTHOR: MARCELLINO ASHRAF ESTAFANOS
=========================================================================
DESCRIPTION
The project is a simple database that can store 10 entries from the user.
Each entry contains 8 pieces of info about a student:
1- Student's ID.
2- Students year.
3- Course 1 ID.
4- Course 1 Grade.
5- Course 2 ID.
6- Course 2 Grade.
7- Course 3 ID.
8- Course 3 Grade.

The user can enter, read, delete and search for any entry he wants.
=========================================================================
**/

#include <stdio.h>
#include <stdlib.h>
#include "Database.h"



int main()
{
    /**Welcome message**/
    printf("Welcome to the simple database program.\n\n");

    int operation = 0;                             /**to choose operation from menu**/
    int year=0;                                    /**to take student's year from user**/
    int ID=0;                                      /**to take student's id from user**/
    int Course_grade[MAX_NUMBER_OF_SUBJECTS]={0};  /**to take all courses' grades from user and pass it's pointer to the functions**/
    int Course_ID[MAX_NUMBER_OF_SUBJECTS]={0};     /**to take all courses' IDs from user and pass it's pointer to the functions**/
    int* ptr_Course_grade=0;                       /**to pass courses' grades to functions**/
    int* ptr_Course_ID=0;                          /**to pass courses' IDs to functions**/
    ptr_Course_grade = Course_grade;               /**assign pointer to array**/
    ptr_Course_ID = Course_ID;                     /**assign pointer to array**/

    int counter = 0;                               /**for loops**/

    data arr_data[MAX_DATA_ENTRIES];               /**to store data structures**/
    ptr_arr_data_start = &arr_data;                /**global pointer to point at the start of the array of data structures**/
    ptr_arr_data_current = &arr_data;              /**global pointer to point at the first empty entry of the array**/
    ptr_arr_data_read = &arr_data;                 /**global pointer to point at the entry to be read without changing the current pointer**/

    /**While loop running forever**/
    while (1)
    {
        /**Menu of operations**/
        printf("Please select the desired operation.\n");
        printf("1\tCheck if the database is full.\n");
        printf("2\tCheck the used size of the database.\n");
        printf("3\tAdd an entry to the database.\n");
        printf("4\tDelete an entry from the database.\n");
        printf("5\tRead an entry from the database.\n");
        printf("6\tGet the list of IDs of the students.\n");
        printf("7\tCheck if an ID exists in the database.\n\n");

        /**store operation number**/
        scanf("%d", &operation);

        /**Check the operation number**/
        if ((operation < 1)||(operation > 7))
        {
            printf("Please enter a valid operation number.\n\n");
            continue;
        }

        switch (operation)
        {
/**====================================================================================**/
        /**Check if the database is full.**/
        case 1:
            if (SDB_IsFull() != 1)
                printf("The database is not full.\n\n");
            else printf("The database is full!\n\n");
            break;
/**====================================================================================**/
        /**Check the used size of the database.**/
        case 2:
            printf("The database currently has %d entries.\n\n", SDB_GetUsedSize());
            break;
/**====================================================================================**/
        /**Add an entry to the database.**/
        case 3:
            /**check if the database is full**/
            if (SDB_IsFull() == 1)
                printf("The database is full.\nPlease delete an entry.\n\n");
            else
            {
                printf("Please enter the entry parameters.\n");
                printf("Student ID    :");
                scanf("%d", &ID);
                printf("Student year  :");
                scanf("%d", &year);
                for (counter = 0; counter < MAX_NUMBER_OF_SUBJECTS; counter++)
                {
                    printf("Course %d ID   :", counter+1);
                    scanf("%d", &Course_ID[counter]);
                    /**Check for grade between 0 and 100**/
                    do
                    {
                        printf("Course %d Grade:", counter+1);
                        scanf("%d", &Course_grade[counter]);
                        if ((Course_grade[counter] > 100) || (Course_grade[counter] < 0))
                            printf("Please enter a valid grade between 0 and 100.\n");
                    } while ((Course_grade[counter] > 100) || (Course_grade[counter] < 0));

                }
                /**Check if the entry succeeded**/
                if (SDB_AddEntry(ID, year, ptr_Course_ID, ptr_Course_grade) == 1)
                    printf("The entry succeeded!\n");
                else printf("The entry failed!\n");
            }
            break;
/**====================================================================================**/
        /**Delete an entry from the database.**/
        case 4:
            printf("Please enter the desired ID to delete.\n");
            printf("ID: ");
            scanf("%d", &ID);
            SDB_DeleteEntry(ID);
            break;
/**====================================================================================**/
        /**Read an entry from the database.**/
        case 5:
            printf("Please enter the desired ID to read its parameters.\n");
            printf("ID: ");
            scanf("%d", &ID);
            SDB_ReadEntry(ID);
            break;
/**====================================================================================**/
        /**Get the list of IDs of the students.**/
        case 6:
            SDB_GetIdList();
            break;
/**====================================================================================**/
        /**Check if an ID exists in the database.**/
        case 7:
            printf("Please enter the ID to check if it exists in the database.\n");
            printf("ID: ");
            scanf("%d", &ID);
            if (SDB_IsIdExist(ID) == 1)
                printf("ID found!\n\n");
            else printf("ID not found.\n\n");
            break;
        }
    }

    return 0;
}
