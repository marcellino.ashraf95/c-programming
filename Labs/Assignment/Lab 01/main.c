/*
======================================================================
Author: Marcellino Ashraf
Name: Lab 01
Target: What is the output of the following code
======================================================================
Solution:
+ the output is "Hello"
+ In case we initializ the cariable (i) with a value 1 the the output
  will be "HiHiHiHiHi..."
======================================================================
*/

#include <stdio.h>
#include <stdlib.h>

int i;
int fun();

int main()
{
    while (i)
    {
        fun();
        main();
    }
    printf("Hello");
    return 0;
}

int fun()
{
    printf("Hi");
}
