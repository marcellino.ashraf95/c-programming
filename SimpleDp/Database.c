#include "Database.h"

/**function to check if the database is full.**/
bool
SDB_IsFull
(void)
{
    if ((ptr_arr_data_current - ptr_arr_data_start) < MAX_DATA_ENTRIES)
        return 0;
    else return 1;
}

/**function to return the number of used entries**/
int
SDB_GetUsedSize
(void)
{
    return (ptr_arr_data_current - ptr_arr_data_start);
}

/**function to add a new entry to the database**/
bool
SDB_AddEntry
(int id,        /**ID of student**/
 int year,      /**Student's year**/
 int* subjects, /**pointer to subjects array**/
 int* grades)   /**pointer to grades array**/
{
    int check = 0, counter; /**check if function completed successfully and counter for loops.**/

    /**make space between text for readability**/
    printf("\n");

    /**assigning data entered to structures using pointers**/
    ptr_arr_data_current->student_ID = id;
    ptr_arr_data_current->student_year = year;
    for(counter = 0; counter < MAX_NUMBER_OF_SUBJECTS; counter++)
    {
        ptr_arr_data_current->Course_Grades[counter] = grades[counter];
        ptr_arr_data_current->Course_ID[counter] = subjects[counter];
    }

    /**increment pointer to the next entry**/
    ptr_arr_data_current++;

    /**if the function reached here then everything is ok!**/
    check = 1;
    if (check == 0) return 0;
    else return 1;
}

/**function to delete entry from database**/
void
SDB_DeleteEntry
(int id)    /**id of entry to be deleted**/
{
    /**Check for id**/
    int counter;
    int old_address_of_ptr_arr_data_current = ptr_arr_data_current;
    for (counter = 0; counter < MAX_DATA_ENTRIES; counter++)
    {
        /**if the id is found then point to this entry**/
        if (id == ((ptr_arr_data_start[counter].student_ID)))
        {
            ptr_arr_data_current = ptr_arr_data_start + counter;
            break;
        }
    }
    /**For loop to move the empty space to the end**/
    for (ptr_arr_data_current;
         ptr_arr_data_current < (old_address_of_ptr_arr_data_current-1);
         ptr_arr_data_current++)
    {
        /**Copy post-structure in the current one**/
        SDB_AddEntry(ptr_arr_data_current[1].student_ID,
                     ptr_arr_data_current[1].student_year,
                     ptr_arr_data_current[1].Course_ID,
                     ptr_arr_data_current[1].Course_Grades);
    }
    ptr_arr_data_current = old_address_of_ptr_arr_data_current;

}

bool
SDB_ReadEntry
(int id)
 {
    int check = 0;
    /**Check for id**/
    int counter;
    if (SDB_IsIdExist(id) == 1)
    {
        for (counter = 0; counter < MAX_DATA_ENTRIES; counter++)
        {
            if (id == ((ptr_arr_data_start[counter].student_ID)))
            {
                ptr_arr_data_read = ptr_arr_data_start + counter;
                break;
            }
        }
    }
    else
    {
        printf("ID not found!\n");
        return 0;
    }

    /**read entry parameters**/
    printf("The entry parameters of the entered ID are:\n");
            printf("Student ID      :%d\n",ptr_arr_data_read->student_ID);
            printf("Student year    :%d\n",ptr_arr_data_read->student_year);
            for(counter = 0; counter < MAX_NUMBER_OF_SUBJECTS; counter++)
            {
                printf("Course %d ID     :%d\n",counter+1, ptr_arr_data_read->Course_ID[counter]);
                printf("Course %d grade  :%d\n",counter+1, ptr_arr_data_read->Course_Grades[counter]);
            }
    check = 1;
    if (check == 1) return 1;
    else return 0;
 }

void
SDB_GetIdList
(void)
{
    printf("Number of valid IDs is: %d\n", SDB_GetUsedSize());
    printf("The valid IDs are:\n");
    int counter;
    for (counter = 0; counter < SDB_GetUsedSize(); counter++)
    {
        printf("ID %d is %d.\n", counter+1, ptr_arr_data_start[counter].student_ID);
    }
    printf("\n");
}

bool
SDB_IsIdExist
(int id)
{
    int check = 0;
    /**Check for id**/
    int counter;
    for (counter = 0; counter < MAX_DATA_ENTRIES; counter++)
    {
        if (id == ((ptr_arr_data_start[counter].student_ID)))
            check = 1;
    }
    return check;
}
