#include <stdio.h>
#include <stdlib.h>

int main()
{
    int sum = 0;
    int i;
    for (i = 0; i <= 1000; i++)
    {
        sum += i;
    }
    printf("The sum of the numbers from 1 to 1000 is %d", sum);
    return 0;
}
