//#define RUN 1
#ifndef CALCULATOR_H_INCLUDED
#define CALCULATOR_H_INCLUDED

#define Max(x,y) x>y?x:y
#define Min(x,y) x>y?y:x



#ifdef RUN
int add(int x, int y);
int subtract(int x, int y);
int multiply(int x, int y);
double divide(int x, int y);
double remainder(int x, int y);
#endif

#endif // CALCULATOR_H_INCLUDED
